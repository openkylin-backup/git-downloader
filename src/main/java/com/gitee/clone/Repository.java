package com.gitee.clone;


public class Repository{
  /**
   * git相关参数设置
   */
  private String org;
  private String accessToken;
  private String type;
  private Integer page;
  private Integer perPage;
  private String Url;

  public String getOrg() {
    return org;
  }

  public void setOrg(String org) {
    this.org = org;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPerPage() {
    return perPage;
  }

  public void setPerPage(Integer perPage) {
    this.perPage = perPage;
  }

  public String getUrl() {
    return Url;
  }

  public void setUrl(String Url) {
    this.Url = Url;
  }

  @Override
  public String toString() {
    return "Repository{" +
            "org='" + org + '\'' +
            ", accessToken='" + accessToken + '\'' +
            ", type='" + type + '\'' +
            ", page=" + page +
            ", perPage=" + perPage +
            ", Url='" + Url + '\'' +
            '}';
  }
}
