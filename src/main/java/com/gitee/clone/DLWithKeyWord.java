package com.gitee.clone;

import com.gitee.ApiException;
import com.gitee.api.GitDataApi;
import com.gitee.api.RepositoriesApi;
import com.gitee.model.Project;
import com.gitee.model.Tree;
import com.gitee.model.TreeBasic;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

/**
 * 下载仓库中文件
 */
public class DLWithKeyWord {
	// 日志
	static String strClassName = GitClone.class.getName();
	static Logger logger = Logger.getLogger(strClassName);

	/**
	 * 根据名字下载仓库中文件
	 *
	 * @param repository 组织仓库的openAPI
	 * @param userName   远程服务器上的用户名
	 * @param passWord   远程服务器上的密码
	 * @param localPath  远程库路径
	 * @param keyWord    搜索关键词
	 * @throws ApiException If fail to call the API
	 */
	public static void dlWithKeyWord(Repository repository, String userName, String passWord, String localPath,
			String keyWord) throws ApiException {

		// 克隆仓库的参数
		RepositoriesApi api = new RepositoriesApi();
		String org = repository.getOrg();
		String accessToken = repository.getAccessToken();
		String type = repository.getType();
		Integer perPage = repository.getPerPage();
		String Url = repository.getUrl();
		int i = 1;

		// 克隆远程仓库
		long startTime = System.currentTimeMillis();
		for (int page = 1;; page++) {
			List<Project> response = api.getV5OrgsOrgRepos(org, accessToken, type, page, perPage);
			for (Project project : response) {
				String fileName = project.getName();
				try {
					searchOnPackage(org, fileName, accessToken, keyWord, Url, localPath);
					logger.info("搜索完成：第" + i++ + "个 : " + org + "/" + fileName);
				} catch (ApiException e) {
					// e.printStackTrace();
					logger.info("正在搜索：第" + i++ + "个 : " + org + "/" + fileName + "时错误： " + e.getMessage() + "\n");
				} catch (IOException e) {
					// e.printStackTrace();
					logger.info("下载 第" + i++ + "个 : " + fileName + "时错误： " + e.getMessage() + "\n");
				}
			}
			if (response.size() < repository.getPerPage()) {
				break;
			}
		}
		long endTime = System.currentTimeMillis();
		logger.info("下载总用时：" + (endTime - startTime) + "ms");

	}

	/**
	 * 在一个包中搜索
	 * 
	 * @param org         包所有者
	 * @param fileName    包名
	 * @param accessToken 授权码
	 * @param keyWord     搜索关键词
	 * @param URL         项目地址
	 * @param localPath   本地储存地址
	 * @throws ApiException 目录列表获取错误
	 * @throws IOException  下载错误
	 */
	public static void searchOnPackage(String org, String fileName, String accessToken, String keyWord, String URL,
			String localPath) throws ApiException, IOException {
		GitDataApi dataApi = new GitDataApi();
		// 遍历一个包的目录，获取所有文件信息
		Tree result = dataApi.getV5ReposOwnerRepoGitTreesSha(org, fileName, "master", accessToken, 1);
		List<TreeBasic> treeList = result.getTree();
		// 新建包本地目录
		makeDir(localPath + "/" + fileName);
		for (TreeBasic t : treeList) {
			if (t.getPath().contains(keyWord) && !t.getType().equals("tree")) {
				// 若为目录则跳过， 否则下载

				String url = URL + "/" + org + "/" + fileName + "/raw/master/";
				String Path = localPath + "/" + fileName + "/";

				downloadFile(url, t.getPath(), Path);

			}
		}
	}

	public static void downloadFile(String url, String path, String localPath) throws IOException {
		URL Url = new URL(url + path);
		HttpURLConnection conn = (HttpURLConnection) Url.openConnection();

		conn.setConnectTimeout(5 * 1000);
		// 回避反拉取
		conn.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36)");

		InputStream is = conn.getInputStream();

		File file = new File(localPath + path);

		String name = path.split("/")[path.split("/").length - 1];
		String dirpath = localPath + path.substring(0, path.length() - name.length());

		makeDir(dirpath);
		FileOutputStream fos = new FileOutputStream(file);

		byte[] cachedBytes = readInputStream(is);
		fos.write(cachedBytes);

		if (fos != null)
			fos.close();
		if (is != null)
			is.close();
		conn.disconnect();

		//logger.info("下载文件：" + path + "  到目录： " + localPath);
	}

	public static void makeDir(String path) {
		File f = new File(path);
		if (!f.exists())
			f.mkdirs();
	}

	public static byte[] readInputStream(InputStream inputStream) throws IOException {
		byte[] buffer = new byte[1024];
		int len = 0;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		while ((len = inputStream.read(buffer)) != -1) {
			bos.write(buffer, 0, len);
		}
		bos.close();
		return bos.toByteArray();
	}

}
