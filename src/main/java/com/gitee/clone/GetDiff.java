package com.gitee.clone;

import com.gitee.ApiException;
import com.gitee.api.GitDataApi;
import com.gitee.api.RepositoriesApi;
import com.gitee.model.Project;
import com.gitee.model.Tree;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * 仓库增量更新
 */
public class GetDiff {
    public static void getDiff(Repository repository, String owner, String sha, Integer recursive, String userName, String passWord, String localPath) throws ApiException, IOException, GitAPIException {
        RepositoriesApi api = new RepositoriesApi();
        GitDataApi dataApi = new GitDataApi();
        Set<String> remoteSet = new HashSet<String>();
        Set<String> localSet = new HashSet<String>();
        Set<String> toRemoveSet = new HashSet<String>();

        String strClassName = GitClone.class.getName();
        Logger logger = Logger.getLogger(strClassName);
        UsernamePasswordCredentialsProvider usernamePasswordCredentialsProvider =
                new UsernamePasswordCredentialsProvider(userName, passWord);
        String org = repository.getOrg();
        String accessToken = repository.getAccessToken();
        String type = repository.getType();
        Integer perPage = repository.getPerPage();
        String Url = repository.getUrl();
        int i = 1;
        for (int page = 1; ; page++) {
            List<Project> response = api.getV5OrgsOrgRepos(org, accessToken, type, page, perPage);
            for (Project project : response) {
                String remotePath = Url + project.getFullName();
                String fileName = project.getName();
                String repo = project.getName();
                Tree result = dataApi.getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive);

                File files = new File(localPath + fileName);
                remoteSet.add(fileName);

                //  1. 判断本地仓库是否已经存在,若已经存在则判断包的内容是否相同
                if (files.exists()) {
                    // 2. 判断本地与远程仓库的内容是否相同,若仓库的内容不同，拉取相应分支上的代码
                    Git git = Git.open(files);
                    org.eclipse.jgit.lib.Repository rep = git.getRepository();
                    Ref ref = rep.getAllRefs().get(Constants.HEAD);
                    String commitId = ref.getObjectId().getName();
                    if (!commitId.equals(result.getSha())) {
                        git.pull().call();
                        logger.info("更新完毕");
                    }                    
                    // 3. 若不存在则clone远程仓库
                } else {
                    Git git = Git.cloneRepository()
                            .setURI(remotePath)
                            .setDirectory(new File(localPath + fileName))
                            .setCredentialsProvider(usernamePasswordCredentialsProvider)
                            .call();
                    logger.info("正在下载：第" + i++ + "个");
                }   
            }
            if (response.size() < repository.getPerPage()) {
                break;
            }
        }

        //获取本地目录下的所有文件
        File file = new File(localPath);
        File[] tempList = file.listFiles();
        for (File LFile : tempList) {
           localSet.add(LFile.getName());
        }
        // 4. 若远程仓库不存在,删除相应的本地仓库
        toRemoveSet.addAll(localSet);
        toRemoveSet.removeAll(remoteSet);
        if (!toRemoveSet.isEmpty()) {
            for (String toRemoveFile : toRemoveSet) {
                deleteFolder(localPath + toRemoveFile);
            }
        }
    }

    public static boolean deleteFolder(String url) {
        File file = new File(url);
        if (!file.exists()) {
            return false;
        }
        if (file.isFile()) {
            file.delete();
            return true;
        } else {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                String root = files[i].getAbsolutePath();
                deleteFolder(root);
            }
            file.delete();
            return true;
        }
    }




}
