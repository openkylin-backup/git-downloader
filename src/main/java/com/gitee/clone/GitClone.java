package com.gitee.clone;


import com.gitee.ApiException;
import com.gitee.api.RepositoriesApi;
import com.gitee.model.Project;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import java.io.*;
import java.util.List;
import java.util.logging.Logger;


/**
 * 克隆远程仓库
 */
public class GitClone {
    /**
     * 克隆远程仓库
     *
     * @param repository 组织仓库的openAPI
     * @param userName   远程服务器上的用户名
     * @param passWord   远程服务器上的密码
     * @param localPath  远程库路径
     * @throws ApiException If fail to call the API
     */
    public static void gitClone(Repository repository, String userName, String passWord, String localPath) throws ApiException {
        // 日志
        String strClassName = GitClone.class.getName();
        Logger logger = Logger.getLogger(strClassName);
        // 克隆仓库的参数
        RepositoriesApi api = new RepositoriesApi();
        UsernamePasswordCredentialsProvider usernamePasswordCredentialsProvider =
                new UsernamePasswordCredentialsProvider(userName, passWord);
        String org = repository.getOrg();
        String accessToken = repository.getAccessToken();
        String type = repository.getType();
        Integer perPage = repository.getPerPage();
        String Url = repository.getUrl();
        int i = 1;
        // 克隆远程仓库
        long startTime = System.currentTimeMillis();
        for (int page = 1;;page++) {
            List<Project> response = api.getV5OrgsOrgRepos(org, accessToken, type, page, perPage);
            for (Project project : response) {
                String remotePath = Url + project.getFullName();
                String fileName = project.getName();
                try {
                    Git git = Git.cloneRepository()
                            .setURI(remotePath)
                            .setDirectory(new File(localPath + fileName))
                            .setCredentialsProvider(usernamePasswordCredentialsProvider)
                            .call();
                    logger.info("正在下载：第" + i++ + "个");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (response.size() < repository.getPerPage()) {
                break;
            }
        }
        long endTime = System.currentTimeMillis();
        logger.info("下载总用时：" + (endTime - startTime) + "ms");

    }
}




