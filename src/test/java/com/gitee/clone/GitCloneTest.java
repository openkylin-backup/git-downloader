package com.gitee.clone;


import com.gitee.ApiException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Test;
import java.io.*;



public class GitCloneTest {
    /**
     * 克隆仓库
     * <p>
     * 克隆仓库
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void test() throws ApiException {
        Repository repository = new Repository();
        repository.setOrg("org_example");   // String | 组织的路径(path/login)
        repository.setAccessToken("accessToken_example");   // String | 用户授权码
        repository.setType("all");  // String | 筛选仓库的类型，可以是 all, public, private。默认: all
        repository.setPerPage(100); // Integer | 每页的数量，最大为 100
        repository.setPage(1); // Integer | 当前的页码
        repository.setUrl("Url_example");    // String | 仓库的路径(path/login)
        GitClone.gitClone(repository, "userName_example", "passWord_example", "localPath_example"); // String | 用户名(userName/login)     String | 密码(passWord/login)     String | 本地下载路径(localPath)

        // TODO: test validations
    }


    /**
     * 仓库增量更新
     *
     * @throws ApiException
     * @throws IOException
     * @throws GitAPIException
     */
    @Test
    public void getDiffTest() throws ApiException, IOException, GitAPIException {
        Repository repository = new Repository();
        repository.setOrg("org_example");   // String | 组织的路径(path/login)
        repository.setAccessToken("accessToken_example");   // String | 用户授权码
        repository.setType("all");  // String | 筛选仓库的类型，可以是 all, public, private。默认: all
        repository.setPerPage(100); // Integer | 每页的数量，最大为 100
        repository.setPage(1); // Integer | 当前的页码
        repository.setUrl("Url_example");    // String | 仓库的路径(path/login)
        String owner = "owner_example";
        String sha = "master";
        Integer recursive = null;
        GetDiff.getDiff(repository, owner, sha, recursive, "userName_example", "passWord_example", "localPath_example");
        // TODO: test validations
    }

}
