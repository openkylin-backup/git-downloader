# git-downloader

## Description
Bulk download Git warehouse to local

## Installation

At first generate the JAR by executing:
    mvn package
Then manually install the following JARs:
* target/git-downloader-1.0-SNAPSHOT.jar
* target/lib/*.jar
java -jar git-downloader-1.0-SNAPSHOT.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.gitee.ApiException;
import com.gitee.api.RepositoriesApi;
import com.gitee.model.Project;
import org.eclipse.jgit.api.Git;


import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.*;
import java.util.List;


/**
 * 克隆远程仓库
 */
public class GitClone{
    /**
     * 克隆远程仓库
     * @param args
     * @throws ApiException If fail to call the API
     */
    public static void main(String[] args) throws ApiException {
        Repository repository = new Repository();
        repository.setOrg("org_example");   // String | 组织的路径(path/login)
        repository.setAccessToken("accessToken_example");   // String | 用户授权码
        repository.setType("all");  // String | 筛选仓库的类型，可以是 all, public, private。默认: all
        repository.setPerPage(100); // Integer | 每页的数量，最大为 100
        repository.setPage(1); // Integer | 当前的页码
        repository.setUrl("Url_example");    // String | 仓库的路径(path/login)
        gitClone(repository,"userName_example","passWord_example","localPath_example"); // String | 用户名(userName/login)     String | 密码(passWord/login)     String | 本地下载路径(localPath)
    }
}
```

## Documentation for API Endpoints

All URIs are relative to *https://gitee.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*GitClone* | [**gitClone**](docs/GitClone.md#gitClone) |  | 克隆远程仓库


<a name="gitClone"></a>
# **gitClone**
> gitClone(repository, userName, passWord, localPath)

克隆远程仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GitClone;


GitClone gitInstance = new GitClone();
String repository = "repository_example"; // String | 组织仓库的openAPI
String userName = "userName_example"; // String | 远程服务器上的用户名
String passWord = "passWord_example"; // String | 远程服务器上的密码
String localPath = "localPath_example"; // String | 远程库路径
try {
    gitInstance.gitClone(repository, userName, passWord, localPath);
} catch (ApiException e) {
    System.err.println("Exception when calling GitClone#gitClone");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository** | **Repository**| 组织仓库的openAPI |
 **userName** | **String**| 远程服务器上的用户名 |
 **passWord** | **String**| 远程服务器上的密码 | 
 **localPath** | **String**| 远程库路径 | 


### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json
